<?php

use Illuminate\Database\Seeder;
use App\Models\Currency;
use App\Http\Controllers\Api\v1\CurrencyApi;

class CurrencyValuesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = (new CurrencyApi())->getCurrencyHistory();
        Currency::insert($items);
    }
}
