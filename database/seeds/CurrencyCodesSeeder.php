<?php

use Illuminate\Database\Seeder;
use App\Http\Controllers\Api\v1\CurrencyApi;
use App\Models\CurrencyCode;

class CurrencyCodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $symbols = (new CurrencyApi())->XML_daily();

        CurrencyCode::insert($symbols);
    }
}
