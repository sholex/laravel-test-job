@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                {{ Form::open(['route' => 'home', 'method' => 'GET'])}}
                <div class="input-group input-group-sm">
                    {{ Form::text('currency_day', request()->currency_day ?? null, ['id' => 'datepicker', 'class' => 'form-control', 'placeholder' => 'select date', 'autocomplete' => 'off']) }}
                    <span class="input-group-btn">
                        {{ Form::submit('Filter', ['btn btn-info btn-flat']) }}
                    </span>
                </div>
                {{ Form::close() }}
            </div>
        </div>
        <div class="row justify-content-center">

            <div class="col-md-12">

                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Currency</th>
                        <th scope="col">CharCode</th>
                        <th scope="col">Value</th>
                        <th scope="col">Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($currencies as $currency)
                    <tr>
                        <td>{{ $currency->CurrencyCodes->name }}</td>
                        <td>{{ $currency->CurrencyCodes->charCode }}</td>
                        <td>{{ $currency->value }}</td>
                        <td>{{ $currency->date }}</td>
                    </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection



