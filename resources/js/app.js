require('./bootstrap');

$('#datepicker').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
    endDate: '+1d'
})
