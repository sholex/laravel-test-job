<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Currency extends Model
{
    public function CurrencyCodes()
    {
        return $this->belongsTo(CurrencyCode::class,'valuteID', 'valuteID');
    }

    public function get_currency_values(Carbon $fromDate, Carbon $toDate, string $valuteID)
    {
        return DB::table('currencies as c')
            ->select('c.valuteID', 'c.value', 'c.date', 'cc.numCode', 'cc.charCode', 'cc.name')
            ->where('c.valuteID', $valuteID)
            ->whereBetween('c.date', [$fromDate, $toDate])
            ->join('currency_codes as cc', 'c.valuteID', '=', 'cc.valuteID')
            ->get();
    }
}
