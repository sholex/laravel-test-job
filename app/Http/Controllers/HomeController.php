<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
//        $currencies = [];
//        if ($request->currency_day){
            $currencies = Currency::with('CurrencyCodes')
                ->where('date', $request->currency_day)
                ->get();
//        }

        return view('welcome', compact(['currencies']));
    }
}
