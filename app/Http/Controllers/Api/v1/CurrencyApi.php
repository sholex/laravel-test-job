<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Resources\CurrencyCollection;
use App\Models\Currency;
use App\Models\CurrencyCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Object_;
use SimpleXMLElement;

class CurrencyApi extends Controller
{
    /**
     * Is used for getting list of all currencies
     * @return array
     */
    public function XML_daily() : array
    {
        $file  = file_get_contents("http://www.cbr.ru/scripts/XML_daily.asp?date_req=02/03/2002");

        $data = new SimpleXMLElement($file);

        foreach ($data as  $item){

            $id = (array) $item['ID'];
            $item = (array) $item;

            $currencies[] = [
                'valuteID' => $id[0],
                'numCode' => $item['NumCode'],
                'charCode' => $item['CharCode'],
                'name' => $item['Name'],
            ];


        }
        return $currencies;
    }

    /**
     * @param string $curId
     * @param string|null $dateFrom
     * @param string|null $dateTo
     * @return SimpleXMLElement
     */
    public function XML_dynamic(string $curId, string $dateFrom = null, string $dateTo = null)
    {
        $dateTo = $dateTo ?? date('d/m/Y');
        $dateFrom = $dateFrom ?? date('d/m/Y', strtotime('-60 day', strtotime(date('Y-m-d'))));

        $url = "http://www.cbr.ru/scripts/XML_dynamic.asp?date_req1=$dateFrom&date_req2=$dateTo&VAL_NM_RQ=$curId";

        $file  = file_get_contents($url);

        return new SimpleXMLElement($file);
    }

    /**
     * @param string|null $dateFrom
     * @param string|null $dateTo
     * @return array
     */
    public function getCurrencyHistory(string $dateFrom = null, string $dateTo = null) : array
    {
        $codes = CurrencyCode::all();

        $table_data = [];

        foreach ($codes as $code){

            $curencyValues = $this->XML_dynamic( $code->valuteID);

            foreach ($curencyValues as $value){
                $value = (array) $value;

                $table_data[] = [
                    'valuteID' => $code->valuteID,
                    'value' => (float) str_replace(",",".",$value['Value']),
                    'date' => Carbon::parse($value['@attributes']['Date'])->format('Y-m-d'),
                ];

            }

        }

        return $table_data;

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCurrencyValues(Request $request)
    {

        $fromDate = Carbon::parse($request->fromDate);
        $toDate   = Carbon::parse($request->toDate);
        $valuteID = $request->valuteID;

        $currencies = DB::table('currencies as c')
            ->select('c.valuteID', 'c.value', 'c.date', 'cc.numCode', 'cc.charCode', 'cc.name')
            ->where('c.valuteID', $valuteID)
            ->whereBetween('c.date', [$fromDate, $toDate])
            ->join('currency_codes as cc', 'c.valuteID', '=', 'cc.valuteID')
            ->get();

        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];

        return response()->json($currencies, 200, $headers, JSON_UNESCAPED_UNICODE);

    }



}
