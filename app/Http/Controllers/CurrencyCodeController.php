<?php

namespace App\Http\Controllers;

use App\Models\CurrencyCode;
use Illuminate\Http\Request;

class CurrencyCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CurrencyCode  $currencyCode
     * @return \Illuminate\Http\Response
     */
    public function show(CurrencyCode $currencyCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CurrencyCode  $currencyCode
     * @return \Illuminate\Http\Response
     */
    public function edit(CurrencyCode $currencyCode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CurrencyCode  $currencyCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CurrencyCode $currencyCode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CurrencyCode  $currencyCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(CurrencyCode $currencyCode)
    {
        //
    }
}
