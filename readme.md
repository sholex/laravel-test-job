## Installation

##### 1 Clone repo in root folder

- `git clone https://sholex@bitbucket.org/sholex/laravel-test-job.git .`

##### 2 Download all dependencies

- `composer update`

- `npm install`

##### 3 Generate application js + css files 

- `npm run dev`

##### 4 Fill database with data

- `php artisan migrate --seed`

## Demo:
login: test@test.ru

pass: 123


### Api usage:

APP_URL/api/v1/get_currency_values?valuteID={ID}&fromDate={DATE}&toDate={DATE}

- ID = valuteID from Database

- DATE = date in YYYY-mm-dd format

For example: 

`/api/v1/get_currency_values?valuteID=R01010&fromDate=2019-06-16&toDate=2019-08-16`

